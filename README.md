# interVASP Messaging Standards
<br>

**IVMS101**
<br>

Universal common language for communication of required originator and beneficiary information between virtual asset service providers.
<br><br>

**interVASP data model standard 
<br>
Issue 1 FINAL**
<br>

[intervasp.org](https://intervasp.org/)
<br><br>

---
<br>

## Contents
<br>

- [**1. Terms and definitions**](#1)
  - [*1.1 List of reserved words and definitions*](#1.1)
  - [*1.2 Abbreviations and Acronyms*](#1.2)
  - [*1.3 Formatting Notations*](#1.3)
  - [*1.4 Naming Conventions*](#1.4)
  - [*1.5 Case sensitivity*](#1.5)
<br><br>

- [**2. Background and rationale**](#2)
  - [*2.1 Background*](#2.1)
  - [*2.2 Rationale*](#2.2)
  - [*2.3 Process Model*](#2.3)
<br><br>

- [**3. Scope of the Standard**](#3)
  - [*3.1 Establish a universal lexicon*](#3.1)
  - [*3.2 Describe the data model*](#3.2)
  - [*3.3 Define constraints over the data*](#3.3)
  - [*3.4 Deliver a model that is fit for global use*](#3.4)
  - [*3.5 Out of scope*](#3.5)
  - [*3.6 Layer model representing scope of IVMS101*](#3.6)
<br><br>

- [**4. Data principles**](#4)
  - [*4.1 Primary Principles*](#4.1)
    - [4.1.1 Adaptability](#4.1.1)
    - [4.1.2 Expandability](#4.1.2)
    - [4.1.3 Fundamentality](#4.1.3)
    - [4.1.4 Portability](#4.1.4)
    - [4.1.5 Individual data points over aggregated or determined values](#4.1.5)
    - [4.1.6 Unambiguous data](#4.1.6)
  - [*4.2 Additional Principles*](#4.2)
    - [4.2.1 Stay within the defined scope](#4.2.1)
    - [4.2.2 Group elements into reusable logical components](#4.2.2)
    - [4.2.3 Data model has been tested with the use of business examples](#4.2.3)
    - [4.2.4 Leverage existing standards work](#4.2.4)
<br><br>

- [**5. Datatypes**](#5)
  - [*5.1 Context for datatypes*](#5.1)
  - [*5.2 Components*](#5.2)
    - [5.2.1 Person](#5.2.1)
    - [5.2.2 NaturalPerson](#5.2.2)
    - [5.2.3 NaturalPersonName](#5.2.3)
    - [5.2.4 NaturalPersonNameID](#5.2.4)
    - [5.2.5 LocalNaturalPersonNameID](#5.2.5)
    - [5.2.6 Address](#5.2.6)
    - [5.2.7 DateAndPlaceOfBirth](#5.2.7)
    - [5.2.8 NationalIdentification](#5.2.8)
    - [5.2.9 LegalPerson](#5.2.9)
    - [5.2.10 LegalPersonName](#5.2.10)
    - [5.2.11 LegalPersonNameID](#5.2.11)
    - [5.2.12 LocalLegalPersonNameID](#5.2.12)
    - [5.2.13 IntermediaryVASP](#5.2.13)
  - [*5.3 Datatypes*](#5.3)
    - [5.3.1 “Max100Text”](#5.3.1)
    - [5.3.2 “LocalMax100Text”](#5.3.2)
    - [5.3.3 “Max50Text”](#5.3.3)
    - [5.3.4 “Max70Text”](#5.3.4)
    - [5.3.5 “Max35Text”](#5.3.5)
    - [5.3.6 “Max16Text”](#5.3.6)
    - [5.3.7 “LEIText”](#5.3.7)
    - [5.3.8 “NaturalPersonNameTypeCode”](#5.3.8)
    - [5.3.9 “LegalPersonNameTypeCode”](#5.3.9)
    - [5.3.10 “Date”](#5.3.10)
    - [5.3.11 “AddressTypeCode”](#5.3.11)
    - [5.3.12 “CountryCode”](#5.3.12)
    - [5.3.13 “RegistrationAuthority”](#5.3.13)
    - [5.3.14 “NationalIdentifierTypeCode”](#5.3.14)
    - [5.3.15 “Number”](#5.3.15)
    - [5.3.16 “TransliterationMethodCode”](#5.3.16)
<br><br>

- [**6. Data model definition**](#6)
  - [*6.1 Context for the data model*](#6.1)
  - [*6.2 Originator*](#6.2)
    - [*6.2.1 Originator structure*](#6.2.1)
    - [*6.2.2 Originator constraints*](#6.2.2)
    - [*6.2.3 Originator elements*](#6.2.3)
  - [*6.3 Beneficiary*](#6.3)
    - [6.3.1 Beneficiary structure](#6.3.1)
    - [6.3.2 Beneficiary constraints](#6.3.2)
    - [6.3.3 Beneficiary elements](#6.3.3)
  - [*6.4 OriginatingVASP*](#6.4)
    - [6.4.1 OriginatingVASP structure](#6.4.1)
    - [6.4.2 OriginatingVASP constraints](#6.4.2)
    - [6.4.3 OriginatingVASP elements](#6.4.3)
  - [*6.5 BeneficiaryVASP*](#6.5)
    - [6.5.1 BeneficiaryVASP structure](#6.5.1)
    - [6.5.2 BeneficiaryVASP constraints](#6.5.2)
    - [6.5.3 BeneficiaryVASP elements](#6.5.3)
  - [*6.6 TransferPath*](#6.6)
    - [6.6.1 TransferPath structure](#6.6.1)
    - [6.6.2 TransferPath constraints](#6.6.2)
    - [6.6.3 TransferPath elements](#6.6.3)
  - [*6.7 PayloadMetadata*](#6.7)
    - [6.7.1 PayloadMetadata structure](#6.7.1)
    - [6.7.2 PayloadMetadata constraints](#6.7.2)
    - [6.7.3 PayloadMetadata elements](#6.7.3)
<br><br>

- [**7. Handling of multiple character sets**](#7)
  - [*7.1 Character set and character encoding*](#7.1)
  - [*7.2 Transformation of national language*](#7.2)
  - [*7.3 Transliteration standards*](#7.3)
<br><br>

- [**8. Business examples**](#8)
  - [*8.1 Example 1*](#8.1)
  - [*8.2 Example 2*](#8.2)
<br><br>

---
<br>

## 1. Terms and definitions <a id="1"></a>
<br>

### 1.1 List of reserved words and definitions <a id="1.1"></a>
<br>

For the purposes of IVMS101, the following terms are considered to be reserved words and follow the definitions below:

<table>
    <tr>
        <th>Term</th>
        <th>Definition</th>
    </tr>
    <tr>
        <td>beneficiary</td>
        <td>refers to the natural or legal person or legal arrangement who is identified by the originator as the receiver of the requested VA transfer.</td>
    </tr>
    <tr>
        <td>beneficiary VASP</td>
        <td>refers to the VASP which receives the transfer of a VA from the originating VASP directly or through an intermediary VASP and makes the funds available to the beneficiary. </br></br> Adapted from General Glossary of the FATF Recommendations</td>
    </tr>
    <tr>
        <td>component</td>
        <td>refers to a composite datatype that consists of one or more elements that can be reused across data entities.</td>
    </tr>
    <tr>
        <td>constraint</td>
        <td>refers to a rule, limitation or control enforced on a datatype, element, component or entity that restricts the content being created or amended.</td>
    </tr>
    <tr>
        <td>country</td>
        <td>refers to a nation with its own government.</td>
    </tr>
    <tr>
        <td>country subdivision</td>
        <td>refers to an administrative subdivision of a country.</td>
    </tr>
    <tr>
        <td>data entity</td>
        <td>refers to a single object in a data model that can be distinctly identified.</td>
    </tr>
    <tr>
        <td>datatype</td>
        <td>refers to an attribute of an element that reflects the possible values that can be represented by the element.</td>
    </tr>
    <tr>
        <td>element</td>
        <td>refers to a unit of data that has precise meaning, serving to identify the attributes of a data entity.</td>
    </tr>
    <tr>
        <td>intermediary VASP</td>
        <td>refers to a VASP in a serial chain that receives and retransmits a VA transfer on behalf of the originating VASP and the beneficiary VASP, or another intermediary VASP. </br></br> Adapted from General Glossary of the FATF Recommendations</td>
    </tr>
    <tr>
        <td>interVASP Messaging</td>
        <td>refers to the transmission of structured data between VASPs and/or other affected entities.</td>
    </tr>
    <tr>
        <td>legal person</td>
        <td>refers to any entity other than a natural person that can establish a permanent customer relationship with an affected entity or otherwise own property. This can include companies, bodies corporate, foundations, anstalt, partnerships, or associations and other relevantly similar entities. </br></br> Adapted from General Glossary of the FATF Recommendations</td>
    </tr>
    <tr>
        <td>legal entity</td>
        <td>refers to a unique party that is legally or financially responsible for the performance of financial transactions or has the legal right in its jurisdiction to enter independently into legal contracts,regardless of whether it is incorporated or constituted in some other way (e.g. trust, partnership, contractual). It includes individuals when acting in a business capacity.</br></br> Adapted from ISO 17442</td>
    </tr>
    <tr>
        <td>locality</td>
        <td>refers to a particular area of a country subdivision, such as a district, suburb, village, town, or city.</td>
    </tr>
    <tr>
        <td>natural person</td>
        <td>refers to a uniquely distinguishable individual; one single person.</td>
    </tr>
    <tr>
        <td>originating VASP</td>
        <td>refers to the VASP which initiates the VA transfer and transfers the VA upon receiving the request for a VA transfer on behalf of the originator. </br></br> Adapted from General Glossary of the FATF Recommendations</td>
    </tr>
    <tr>
        <td>originator</td>
        <td>refers to the account holder who allows the VA transfer from that account or, where there is no account, the natural or legal person that places the order with the originating VASP to perform the VA transfer. </br></br> Adapted from General Glossary of the FATF Recommendations</td>
    </tr>
    <tr>
        <td>other affected entity</td>
        <td>refers to a natural or legal person, other than a VASP, that engages in or provides covered VA activities.</td>
    </tr>
    <tr>
        <td>Recommendation</td>
        <td>refers to a FATF Recommendation.</td>
    </tr>
    <tr>
        <td>registration authority</td>
        <td>refers to a corporate or business registry, or other national or local authority that maintains the authoritative source of information about legal entities operating in its jurisdiction.</td>
    </tr>
    <tr>
        <td>Standard</td>
        <td>refers to the IVMS101 standard.</td>
    </tr>
    <tr>
        <td>transfer</td>
        <td>refers to conducting a transaction on behalf of another natural or legal person that moves a virtual asset from one virtual asset address or account to another. </br></br> Source: FATF guidance for a risk-based approach to virtual assets and virtual asset service providers</td>
    </tr>
    <tr>
        <td>virtual asset</td>
        <td>refers to a digital representation of value that can be digitally traded, or transferred, and can be used for payment or investment purposes. Virtual assets do not include digital representations of fiat currencies, securities and other financial assets that are already covered elsewhere in the FATF Recommendations. </br></br> Source: General Glossary of the FATF Recommendations</td>
    </tr>
    <tr>
        <td>virtual asset service provider</td>
        <td>refers to any natural or legal person who is not covered elsewhere under the FATF Recommendations, and as a business conducts one or more of the following activities or operations for or on behalf of another natural or legal person: 
        </br>i) exchange between virtual assets and fiat currencies; 
        </br>ii) exchange between one or more forms of virtual assets; 
        </br>iii) transfer of virtual assets; 
        </br>iv) safekeeping and/or administration of virtual assets or instruments enabling control over virtual assets; and 
        </br>v) participation in and provision of financial services related to an issuer’s offer and/or sale of a virtual asset. 
        </br></br>Source: General Glossary of the FATF Recommendations</td>
    </tr>
</table>
<br><br>

### 1.2 Abbreviations and Acronyms <a id="1.2"></a>
<br>

The following is a list of abbreviations and acronyms used in this document:

<table>
    <tr>
        <th>Abbreviation/Acronym</th>
        <th>Definition</th>
    </tr>
    <tr>
        <td>AML</td>
        <td>anti-money laundering</td>
    </tr>
    <tr>
        <td>CDD</td>
        <td>customer due diligence</td>
    </tr>
    <tr>
        <td>CFT</td>
        <td>countering the financing of terrorism</td>
    </tr>
    <tr>
        <td>FATF</td>
        <td>Financial Action Task Force</td>
    </tr>
    <tr>
        <td>GLEIF</td>
        <td>Global Legal Entity Identifier Foundation</td>
    </tr>
    <tr>
        <td>IVMS</td>
        <td>interVASP Messaging Standard</td>
    </tr>
    <tr>
        <td>ISO</td>
        <td>International Organisation for Standardisation</td>
    </tr>
    <tr>
        <td>KYC</td>
        <td>know your customer</td>
    </tr>
    <tr>
        <td>LEI</td>
        <td>Legal Entity Identifier</td>
    </tr>
    <tr>
        <td>VA</td>
        <td>virtual asset</td>
    </tr>
    <tr>
        <td>VASP</td>
        <td>virtual asset service provider</td>
    </tr>
    <tr>
        <td>TSP</td>
        <td>technical solution provider</td>
    </tr>
</table>
<br><br>

### 1.3 Formatting Notations <a id="1.3"></a>
<br>

Entities and components will follow an upper camel case convention with no spaces, with the first word capitalised.

Elements will follow a lower camel case convention with no spaces; the first word will not be capitalised. Datatypes will be referred to in upper camel case, unless convention requires otherwise.

For readability purposes, entities may be referred to in their business context in Title Case, which will permit the use of spaces between words. 
<br><br>

### 1.4 Naming Conventions <a id="1.4"></a>
<br>

Any term used within an entity, component or element name does not necessarily dictate the value for such an object. For example, customerIdentifier, and other elements where the name contains the word *‘Number’*, do not necessarily have to only consist of numerals. Valid values are determined through the datatype definition and any applicable constraints.
<br><br>

### 1.5 Case sensitivity <a id="1.5"></a>
<br>

Unless otherwise specified, all values are case insensitive.
<br><br><br>

---
<br>

## 2. Background and rationale <a id="2"></a>
<br>

### 2.1 Background <a id="2.1"></a>
<br>

In October 2018, FATF adopted changes to its Recommendations to explicitly clarify that they apply to financial activities involving VA, effectively expanding the scope of the Recommendations to apply to VASPs and other obliged entities that engage in or provide covered VA activities (together, affected entities).

INR. 15, paragraph 7(b), relating to Recommendation 16[<sup>1</sup>](#ref-1) was officially adopted to form part of the FATF standards in June 2019 stating that countries should ensure that:

> *“…originating VASPs obtain and hold required and accurate originator information and required beneficiary information on virtual asset transfers, submit this information to beneficiary VASP or financial institution (if any) immediately and securely.*
>
>*…beneficiary VASPs obtain and hold required originator information and required and accurate beneficiary information…”*

Originating VASPs must transmit mandated data to the Beneficiary VASP (if applicable) immediately and securely, ensuring that only those parties processing the transfer have access to the information.

While it is already expected that traditional financial institutions share this information between counterparties, numerous technical, operational and cultural challenges brought about by this clarification are unique to the VA space. The variances between the data that are captured by VASPs, and shared over existing blockchain networks by which VA are transferred locally and globally create a technical challenge that does not impact traditional payments gateways.

The diverse AML/CFT regulatory landscape applying to the VA space has also led to notable differences in the client data already possessed by VASPs between jurisdictions. For all practical purposes, VASPs operate in a global ecosystem and as such, local and regional data compliance and privacy laws with which personal data can be shared and persisted apply. Globalisation challenges, including the use of data captured in multiple languages, character sets and local conventions, can affect the sharing of data between VASPs.
<br><br>

### 2.2 Rationale <a id="2.2"></a>
<br>

Consistency and harmonisation are in the interests of all VASPs. This is particularly the case in the field of domestic and cross-border messaging between VASPs. There exists a need for VASPs to adopt uniform approaches and establish best market practice, especially in relation to meeting obligations resulting from the FATF Recommendations as they apply to VASPs.

Unambiguous data allows VASPs to exchange messages in an automated fashion, reducing costs and minimising risks. A universal common language enables beneficiary VASPs to understand and process required originator and beneficiary information submitted by originating VASPs.

Following the update to FATF paragraph INR.15 7(b)-R.16, several technical solutions that facilitate the transfer of such data between VASPs have been progressed by TSPs to aid VASPs in meeting their regulatory obligations.

As such, there arises a greater need for the standardisation of the data domain to which VASPs can share data to avoid the proliferation of numerous data models. While underlying source data may not align to the Standard, standardisation of the data domain to which the required data is shared can ease the adoption of such technical solutions by VASPs. Irrespective of the
technical solution, data should be expected to be structured in line with the Standard at time of transmission.

When consumed, the data for entities covered by the Standard can be received in a structure that is meaningful, well-formed and complete. The technical burden towards adoption can be eased, as a VASP has only to develop transformations for such entities once.

Through the introduction of the Standard, TSPs can focus less on having to model the business domain, assigning resources to deliver the technical solution.

Through the alignment of data structures and those constraints governing the content, sharing or consuming data that is compliant with the Standard can be assured of a quality of data that may be lost should there be numerous models to which the platforms would operate. By assuring the reliability of data, it can be expected that data be obtained and maintained to a higher standard than would be the case without the Standard.
<br><br>

### 2.3 Process Model <a id="2.3"></a>
<br>

<figure>
  <img src="process-model.png" alt="Process model image">
  <figcaption>Figure 1: Transfer of data between an originator and beneficiary by way of an originating, intermediary and beneficiary VASP</figcaption>
</figure>
<br><br><br>

---
<br>

## 3. Scope of the Standard <a id="3"></a>
<br>

To meet the challenge as described in Section 2.1 (Background), the Standard covers four primary areas in the sharing of data, agreed through consensus with the Joint Working Group on interVASP Messaging Standards.
<br>

### 3.1 Establish a universal lexicon <a id="3.1"></a>
<br>

In a business domain with countless acronyms, definitions and synonyms, identifying a universal lexicon, or common vocabulary, for those terms and concepts that constitute the data universe allows for the reduction of ambiguity and risk of misinterpretation of data that must be shared between parties.
<br><br>

### 3.2 Describe the data model <a id="3.2"></a>
<br>

The data model consists of an agreed representation of format, definition and structure of those entities that form the business domain. A standardised data model also serves to reduce duplication of effort on take-up of TSPs, improving reliability and reducing effort and costs of meeting regulatory obligations in the sharing of data between VASPs.
<br><br>

### 3.3 Define constraints over the data <a id="3.3"></a>
<br>

Appropriate restrictions over permissible content may not ensure the correctness of the data but will ensure the completeness and well-formedness of any data that is shared. It is through the adherence to constraints that data retrieval can be in line with expectations and data quality upheld.
<br><br>

### 3.4 Deliver a model that is fit for global use <a id="3.4"></a>
<br>

Legacy client data will have been captured by VASPs in numerous languages, character sets and structural conventions. Sharing this data across borders, to actors potentially spanning numerous geographies, requires conventions that will ensure that the format, as intended by the originator can be consumed in an expected, meaningful manner by all required VASPs.
<br><br>

### 3.5 Out of scope <a id="3.5"></a>
<br>

It is beyond the scope of the Standard to describe mechanisms that will cater to:
- Information security;
- Data privacy and global compliance requirements;
- Identification of VASPs or other affected entities;
- Representation of verification status or mechanism of a VASP or other entity (for example, CDD/KYC data or other forms of due diligence data that is implementation, TSP or VASP specific);
- Network protocols, or other component leveraged in the mode of transmission;
- Storage of any data included in the payload by the originating or beneficiary VASP;
- The manner in which payload data is leveraged by the beneficiary VASP for the purposes of Recommendation 16, such as the requirement for a VASP to <i>identify and report suspicious transactions, monitor the availability of information, take freezing actions, and prohibit transactions with designated persons and entities.</i>
<br><br>

### 3.6 Layer model representing scope of IVMS101 <a id="3.6"></a>
<br>

The following diagram is intended to serve as a conceptual model characterising the partition
of a technology platform to which the scope of IVMS101 applies[<sup>2</sup>](#ref-2). 
<figure>
  <img src="layer-model.png" alt="Layer model image">
  <figcaption>Figure 2: Conceptual model illustrating scope of IVMS101</figcaption>
</figure>
<br><br><br>

---
<br>

## 4. Data principles <a id="4"></a>
<br>

### 4.1 Primary Principles <a id="4.1"></a>
<br>

#### 4.1.1 Adaptability <a id="4.1.1"></a>
<br>

The IVMS101 data model is able to withstand enhancement or correction and will not compromise the integrity of instances where data is structured in accordance with the Standard has already been shared or held.
<br><br>

#### 4.1.2 Expandability <a id="4.1.2"></a>
<br>

The IVMS101 model is expandable to support additional requirements from the Standard, and also to permit it being used as a baseline from which subsequent data standards can be prepared.
<br><br>

#### 4.1.3 Fundamentality <a id="4.1.3"></a>
<br>

The IVMS101 model provides the fundamental structures and rules related to the business entities that are deemed required for transmission between VASPs. As a design principle, the model may be wrapped in data structures supporting the implementation, should it be required. However, while the Standard does not preclude features and functionality from a platform that leverages the model, it will not incorporate proprietary or implementation specific objects or rules.
<br><br>

#### 4.1.4 Portability <a id="4.1.4"></a>
<br>

The IVMS101 model is technology neutral. It can be codified across markup languages, notations and document specifications, and can be supported by disparate systems.
<br><br>

#### 4.1.5 Individual data points over aggregated or determined values <a id="4.1.5"></a>
<br>

The IVMS101 model includes individual, granular data over aggregate or determined values. Where required, source data that would serve as inputs into determination logic is preferred over the output of such logic.
<br><br>

#### 4.1.6 Unambiguous data <a id="4.1.6"></a>
<br>

The IVMS101 model holds data that is specific and not subject to interpretation. Through adherence to the Standard, the consumer of transmitted data will receive it as intended by the originating VASP.
<br><br>

### 4.2 Additional Principles <a id="4.2"></a>
<br>

#### 4.2.1 Stay within the defined scope <a id="4.2.1"></a>
<br>

In line with the scope set out in Section 3 (Scope of the Standard), the data model adheres to the following:
- The entity, component, attribute or relationship (if deemed necessary) is within the stated scope of IVMS101.
- The entity, component, attribute or relationship is necessary for the purposes of meeting the business challenge as stated in Section 2 (Background and rationale).
- The entity, component, attribute or relationship is specifically required to apply context to the data transmitted to meet the business challenge as stated in Section 2 (i.e. the data would be meaningless or ambiguous without such information).
<br><br>

#### 4.2.2 Group elements into reusable logical components <a id="4.2.2"></a>
<br>

Elements are grouped into reusable components. Through the grouping of elements into reusable components, it is possible to avoid the duplication of efforts in oversight of each element as it applies to each component.
<br><br>

#### 4.2.3 Data model has been tested with the use of business examples <a id="4.2.3"></a>
<br>

The data model is tested, both in its initial release and as it evolves, with real business examples to demonstrate the correctness and completeness of the data that may be transmitted.
<br><br>

#### 4.2.4 Leverage existing standards work <a id="4.2.4"></a>
<br>

IVMS101 leverages relevant and appropriate standards, i.e. standards, by whatever name, issued, adopted and/or published by a standards-setting body.
<br><br><br>

---
<br>

## 5. Datatypes <a id="5"></a>
<br>

### 5.1 Context for datatypes <a id="5.1"></a>
<br>

This section describes the components and datatypes leveraged in those entities documented in Section 6 (Data model definition). Definitions to the terms used are available in Section 1.1, (Terms and Definitions).

Figure 3, IVMS data model taxonomy (1) represents in *blue* the objects within the IVMS101 data model taxonomy that are documented in this section.
<figure>
  <img src="context-of-datatypes.png" alt="Context of datatypes">
  <figcaption>Figure 3: IVMS101 data model taxonomy (1) component through to datatype</figcaption>
</figure>
<br><br>

### 5.2 Components <a id="5.2"></a>
<br>

#### 5.2.1 Person <a id="5.2.1"></a>

##### 5.2.1.1 *Person structure*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>naturalPerson</td>
        <td>[0..1]</td>
        <td>NaturalPerson</td>
        <td>C1, C2, C3, C6, C8</td>
        <td>{Or</td>
    </tr>
    <tr>
        <td>legalPerson</td>
        <td>[0..1]</td>
        <td>LegalPerson</td>
        <td>C3, C4, C5, C7, C8, C9, C10, C11</td>
        <td>Or}</td>
    </tr>
</table>
<br>

##### 5.2.1.2 *Person constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C1 &nbsp; OriginatorInformationNaturalPerson**
<br>
If the originator is a NaturalPerson either (geographicAddress with an addressType value of ‘GEOG’ or ‘HOME’ or ‘BIZZ’) and/or customerIdentifier and/or nationalIdentification and/or dateAndPlaceOfBirth is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C2 &nbsp; DateInPast**
<br>
If dateOfBirth is specified, the date specified must be a historic date (i.e. a date prior to the current date).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C4 &nbsp; OriginatorInformationLegalPerson**
<br>
If the originator is a LegalPerson either (geographicAddress with an addressType value of ‘GEOG’) and/or customerIdentifier and/or nationalIdentification is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C5 &nbsp; LegalNamePresentLegalPerson**
<br>
At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’ specified in the element *legalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C6 &nbsp; LegalNamePresentNaturalPerson**
<br>
At least one occurrence of naturalPersonNameID must have the value ‘LEGL’ specified in the element *naturalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C7 &nbsp; ValidNationalIdentifierLegalPerson**
<br>
A legal person must have a value for nationalIdentifierType of either ‘RAID’ or ‘MISC’ or ‘LEIX’ or ‘TXID’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C8 &nbsp; ValidAddress**
<br>
There must be at least one occurrence of the element addressLine or (streetName and buildingName and/or buildingNumber).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C9 &nbsp; CompleteNationalIdentifierLegalPerson**
<br>
A LegalPerson must not have a value for countryOfIssue and (must have a value for the element RegistrationAuthority if the value for nationalIdentifierType is not ‘LEIX’ and must not have a value for the element RegistrationAuthority if the value for nationalIdentifierType is ‘LEIX’).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C10 &nbsp; RegistrationAuthority**
<br>
The value used for the applicable element must be present on the GLEIF Registration Authorities List.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C11 &nbsp; ValidLEI**
<br>
A LegalPerson with a nationalIdentifierType of ‘LEIX’ must have a value for the element nationalIdentifier that adheres to the convention as stated in datatype ‘LEIText’.
<br>

##### 5.2.1.3 *Person elements*

##### 5.2.1.3.1 naturalPerson
Multiplicity: [0..1]
<br>
Definition: refers to a uniquely distinguishable individual; one single person.
<br>
Component: NaturalPerson
<br>

##### 5.2.1.3.2 legalPerson
Multiplicity: [0..1]
<br>
Definition: refers to any entity other than a natural person that can establish a permanent customer relationship with an affected entity or otherwise own property. This can include companies, bodies corporate, foundations, anstalt, partnerships, or associations and other relevantly similar entities.
<br>
Component: LegalPerson 
<br><br>

#### 5.2.2 NaturalPerson <a id="5.2.2"></a>

##### 5.2.2.1 *NaturalPerson structure*
<table>
    <tr>
        <th>Element</td>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>name</td>
        <td>[1..n]</td>
        <td>NaturalPersonName</td>
        <td>C6</td>
        <td></td>
    </tr>
    <tr>
        <td>geographicAddress</td>
        <td>[0..n]</td>
        <td>Address</td>
        <td>C1, C3, C8</td>
        <td></td>
    </tr>
    <tr>
        <td>nationalIdentification</td>
        <td>[0..1]</td>
        <td>NationalIdentification</td>
        <td>C1, C3</td>
        <td></td>
    </tr>
    <tr>
        <td>customerIdentification</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td>C1</td>
        <td></td>
    </tr>
    <tr>
        <td>dateAndPlaceOfBirth</td>
        <td>[0..1]</td>
        <td>DateAndPlaceOfBirth</td>
        <td>C1, C2</td>
        <td></td>
    </tr>
    <tr>
        <td>countryOfResidence</td>
        <td>[0..1]</td>
        <td>CountryCode</td>
        <td>C3</td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.2.2 *NaturalPerson constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C1 &nbsp; OriginatorInformationNaturalPerson**
<br>
If the originator is a NaturalPerson either (geographicAddress with an addressType value of ‘GEOG’ or ‘HOME’ or ‘BIZZ’) and/or customerIdentifier and/or nationalIdentification and/or dateAndPlaceOfBirth is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C2 &nbsp; DateInPast**
<br>
If dateOfBirth is specified, the date specified must be a historic date (i.e. a date prior to the current date).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C6 &nbsp; LegalNamePresentNaturalPerson**
<br>
At least one occurrence of naturalPersonNameID must have the value ‘LEGL’ specified in the element *naturalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C8 &nbsp; ValidAddress**
<br>
There must be at least one occurrence of the element addressLine or (streetName and buildingName and/or buildingNumber).
<br>

##### 5.2.2.3 *NaturalPerson elements*

##### 5.2.2.3.1 name
Multiplicity: [1..1]
<br>
Definition: the distinct words used as identification for an individual.
<br>
Component: NaturalPersonName
<br>

##### 5.2.2.3.2 geographicAddress
Multiplicity: [0..n]
<br>
Definition: the particulars of a location at which a person may be communicated with.
<br>
Component: Address
<br>

##### 5.2.2.3.3 nationalIdentification
Multiplicity: [0..1]
<br>
Definition: a distinct identifier used by governments of countries to uniquely identify a natural or legal person.
<br>
Component: NationalIdentification
<br>

##### 5.2.2.3.4 customerIdentification
Multiplicity: [0..1]
<br>
Definition: a distinct identifier that uniquely identifies the person to the institution in context.
<br>
Datatype: “Max50Text”
<br>

##### 5.2.2.3.5 dateAndPlaceOfBirth
Multiplicity: [0..1]
<br>
Definition: date and place of birth of a person.
<br>
Component: DateAndPlaceOfBirth
<br>

##### 5.2.2.3.6 countryOfResidence
Multiplicity: [0..1]
<br>
Definition: country in which a person resides (the place of a person's home).
<br>
Datatype: “CountryCode”
<br><br>

#### 5.2.3 NaturalPersonName <a id="5.2.3"></a>

##### 5.2.3.1 *NaturalPersonName structure*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>nameIdentifier</th>
        <td>[1..n]</td>
        <td>NaturalPersonNameID</td>
        <td>C6</td>
        <td></td>
    </tr>
    <tr>
        <td>localNameIdentifier</th>
        <td>[0..n]</td>
        <td>LocalNaturalPersonNameID</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>phoneticNameIdentifier</th>
        <td>[0..n]</td>
        <td>LocalNaturalPersonNameID</td>
        <td></td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.3.2 *NaturalPersonName constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C6 &nbsp; LegalNamePresentNaturalPerson**
<br>
At least one occurrence of naturalPersonNameID must have the value ‘LEGL’ specified in the element *naturalPersonNameIdentifierType*.
<br>

##### 5.2.3.3 *NaturalPersonName elements*

##### 5.2.3.3.1 nameIdentifier
Multiplicity: [1..n]
<br>
Definition: full name separated into primary and secondary identifier.
<br>
Component: NaturalPersonNameID
<br>

##### 5.2.3.3.2 localNameIdentifier
Multiplicity: [0..n]
<br>
Definition: full name separated into primary and secondary identifier using local characters.
<br>
Component: LocalNaturalPersonNameID
<br>

##### 5.2.3.3.3 phoneticNameIdentifier
Multiplicity: [0..n]
<br>
Definition: Alternate representation of a name that corresponds to the manner the name is pronounced.
<br>
Component: LocalNaturalPersonNameID
<br><br>

#### 5.2.4 NaturalPersonNameID <a id="5.2.4"></a>

##### 5.2.4.1 *NaturalPersonNameID structure[<sup>3</sup>](#ref-3)*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>primaryIdentifier</td>
        <td>[1..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>secondaryIdentifier</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>nameIdentifierType</td>
        <td>[1..1]</td>
        <td>SingleValueFromList</td>
        <td>C6</td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.4.2 *NaturalPersonNameID constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C6 &nbsp; LegalNamePresentNaturalPerson**
<br>
At least one occurrence of naturalPersonNameID must have the value ‘LEGL’ specified in the element *naturalPersonNameIdentifierType*.
<br>

##### 5.2.4.3 *NaturalPersonNameID elements*

##### 5.2.4.3.1 primaryIdentifier
Multiplicity: [1..1]
<br>
Definition: This may be the family name, the maiden name or the married name, the main name, the surname, and in some cases, the entire name where the natural person’s name cannot be divided into two parts, or where the sender is unable to divide the natural person’s name into two parts.
<br>
Datatype: “Max100Text”
<br>

##### 5.2.4.3.2 secondaryIdentifier
Multiplicity: [0..1]
<br>
Definition: These may be the forenames, familiar names, given names, initials, prefixes, suffixes or Roman numerals (where considered to be legally part of the name) or any other secondary names.
<br>
Datatype: “Max100Text”
<br>

##### 5.2.4.3.3 nameIdentifierType
Multiplicity: [1..1]
<br>
Definition: The nature of the name specified.
<br>
Datatype: “NaturalPersonNameTypeCode”
<br><br>

#### 5.2.5 LocalNaturalPersonNameID <a id="5.2.5"></a>

##### 5.2.5.1 *LocalNaturalPersonNameID structure[<sup>4</sup>](#ref-4)*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>primaryIdentifier</td>
        <td>[1..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>secondaryIdentifier</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>nameIdentifierType</td>
        <td>[1..1]</td>
        <td>SingleValueFromList</td>
        <td></td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.5.2 *LocalNaturalPersonNameID constraints*
<br>

There are no constraints that apply to the LocalNaturalPersonNameID component.
<br>

##### 5.2.5.3 *LocalNaturalPersonNameID elements*

##### 5.2.5.3.1 primaryIdentifier
Multiplicity: [1..1]
<br>
Definition: This may be the family name, the maiden name or the married name, the main name, the surname, and in some cases, the entire name where the natural person’s name cannot be divided into two parts, or where the sender is unable to divide the natural person’s name into two parts.
<br>
Datatype: “LocalMax100Text”
<br>

##### 5.2.5.3.2 secondaryIdentifier
Multiplicity: [0..1]
<br>
Definition: These may be the forenames, familiar names, given names, initials, prefixes, suffixes or Roman numerals (where considered to be legally part of the name) or any other secondary names.
<br>
Datatype: “LocalMax100Text”
<br>

##### 5.2.5.3.3 nameIdentifierType
Multiplicity: [1..1]
<br>
Definition: The nature of the name specified.
<br>
Datatype: “NaturalPersonNameTypeCode”
<br><br>

#### 5.2.6 Address <a id="5.2.6"></a>

##### 5.2.6.1 *Address structure[<sup>5</sup>](#ref-5)*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>addressType</td>
        <td>[1..1]</td>
        <td>SingleValueFromList</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>department</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>subDepartment</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>streetName</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td>C8</td>
        <td></td>
    </tr>
    <tr>
        <td>buildingNumber</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td>C8</td>
        <td></td>
    </tr>
    <tr>
        <td>buildingName</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td>C8</td>
        <td></td>
    </tr>
    <tr>
        <td>floor</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>postBox</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>room</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>postcode</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>townName</td>
        <td>[1..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>townLocationName</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>districtName</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>countrySubDivision</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>addressLine</td>
        <td>[0..7]</td>
        <td>Text</td>
        <td>C8</td>
        <td></td>
    </tr>
    <tr>
        <td>country</td>
        <td>[1..1]</td>
        <td>CountryCode</td>
        <td>C3</td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.6.2 *Address constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C8 &nbsp; ValidAddress**
<br>
There must be at least one occurrence of the element addressLine or (streetName and buildingName and/or buildingNumber).
<br>

##### 5.2.6.3 *Address elements*

##### 5.2.6.3.1 addressType
Multiplicity: [1..1]
<br>
Definition: Identifies the nature of the address.
<br>
Datatype: “AddressTypeCode”
<br>

##### 5.2.6.3.2 department
Multiplicity: [0..1]
<br>
Definition: Identification of a division of a large organisation or building.
<br>
Datatype: “Max50Text”
<br>

##### 5.2.6.3.3 subDepartment
Multiplicity: [0..1]
<br>
Definition: Identification of a sub-division of a large organisation or building.
<br>
Datatype: “Max70Text”
<br>

##### 5.2.6.3.4 streetName
Multiplicity: [0..1]
<br>
Definition: Name of a street or thoroughfare.
<br>
Datatype: “Max70Text”
<br>

##### 5.2.6.3.5 buildingNumber
Multiplicity: [0..1]
<br>
Definition: Number that identifies the position of a building on a street.
<br>
Datatype: “Max16Text”
<br>

##### 5.2.6.3.6 buildingName
Multiplicity: [0..1]
<br>
Definition: Name of the building or house.
<br>
Datatype: “Max35Text”
<br>

##### 5.2.6.3.7 floor
Multiplicity: [0..1]
<br>
Definition: Floor or storey within a building.
<br>
Datatype: “Max70Text”
<br>

##### 5.2.6.3.8 postBox
Multiplicity: [0..1]
<br>
Definition: Numbered box in a post office, assigned to a person or organisation, where letters are kept until called for.
<br>
Datatype: “Max16Text”
<br>

##### 5.2.6.3.9 room
Multiplicity: [0..1]
<br>
Definition: Building room number.
<br>
Datatype: “Max70Text”
<br>

##### 5.2.6.3.10 postCode
Multiplicity: [0..1]
<br>
Definition: Identifier consisting of a group of letters and/or numbers that is added to a postal address to assist the sorting of mail.
<br>
Datatype: “Max16Text”
<br>

##### 5.2.6.3.11 townName
Multiplicity: [0..1]
<br>
Definition: Name of a built-up area, with defined boundaries, and a local government.
<br>
Datatype: “Max35Text”
<br>

##### 5.2.6.3.12 townLocationName
Multiplicity: [0..1]
<br>
Definition: Specific location name within the town.
<br>
Datatype: “Max35Text”
<br>

##### 5.2.6.3.13 districtName
Multiplicity: [0..1]
<br>
Definition: Identifies a subdivision within a country subdivision.
<br>
Datatype: “Max35Text”
<br>

##### 5.2.6.3.14 countrySubDivision
Multiplicity: [0..1]
<br>
Definition: Identifies a subdivision of a country for example, state, region, province, départment or county.
<br>
Datatype: “Max35Text”
<br>

##### 5.2.6.3.15 addressLine
Multiplicity: [0..7]
<br>
Definition: Information that locates and identifies a specific address, as defined by postal services, presented in free format text.
<br>
Datatype: “Max70Text”
<br>

##### 5.2.6.3.16 country
Multiplicity: [1..1]
<br>
Definition: Nation with its own government.
<br>
Datatype: “CountryCode”
<br><br>

#### 5.2.7 DateAndPlaceOfBirth <a id="5.2.7"></a>

##### 5.2.7.1 *DateAndPlaceOfBirth structure[<sup>6</sup>](#ref-6)*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>dateOfBirth</td>
        <td>[1..1]</td>
        <td>Date</td>
        <td>C2</td>
        <td></td>
    </tr>
    <tr>
        <td>placeOfBirth </td>
        <td>[1..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.7.2 *DateAndPlaceOfBirth constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C2 &nbsp; DateInPast**
<br>
If dateOfBirth is specified, the date specified must be a historic date (i.e. a date prior to the current date)
<br>

##### 5.2.7.3 *DateAndPlaceOfBirth elements*

##### 5.2.7.3.1 dateOfBirth
Multiplicity: [1..1]
<br>
Definition: Date on which a person is born.
<br>
Datatype: Date
<br>

##### 5.2.7.3.2 placeOfBirth
Multiplicity: [1..1]
<br>
Definition: The town and/or the city and/or the suburb and/or the country subdivision and/or the country where the person was born.
<br>
Datatype: “Max70Text”
<br><br>

#### 5.2.8 NationalIdentification <a id="5.2.8"></a>

##### 5.2.8.1 *NationalIdentification structure*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>nationalIdentifier</td>
        <td>[1..1]</td>
        <td>Text</td>
        <td>C11</td>
        <td></td>
    </tr>
    <tr>
        <td>nationalIdentifierType</td>
        <td>[1..1]</td>
        <td>SingleValueFromList</td>
        <td>C7, C9, C11</td>
        <td></td>
    </tr>
    <tr>
        <td>countryOfIssue</td>
        <td>[0..1]</td>
        <td>CountryCode</td>
        <td>C3</td>
        <td></td>
    </tr>
    <tr>
        <td>registrationAuthority</td>
        <td>[0..1]</td>
        <td>RegistrationAuthority</td>
        <td>C9, C10</td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.8.2 *NationalIdentification constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C7 &nbsp; ValidNationalIdentifierLegalPerson**
<br>
A legal person must have a value for nationalIdentifierType of either ‘RAID’ or ‘MISC’ or ‘LEIX’ or ‘TXID’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C9 &nbsp; CompleteNationalIdentifierLegalPerson**
<br>
A LegalPerson must not have a value for countryOfIssue and (must have a value for the element RegistrationAuthority if the value for nationalIdentifierType is not ‘LEIX’ and must not have a value for the element RegistrationAuthority if the value for nationalIdentifierType is ‘LEIX’).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C10 &nbsp; RegistrationAuthority**
<br>
The value used for the applicable element must be present on the GLEIF Registration Authorities List.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C11 &nbsp; ValidLEI**
<br>
A LegalPerson with a nationalIdentifierType of ‘LEIX’ must have a value for the element nationalIdentifier that adheres to the convention as stated in datatype ‘LEIText’.
<br>

##### 5.2.8.3 *NationalIdentification elements*

##### 5.2.8.3.1 nationalIdentifier
Multiplicity: [1..1]
<br>
Definition: An identifier issued by an appropriate issuing authority.
<br>
Datatype: “Max35Text”
<br>

##### 5.2.8.3.2 nationalIdentifierType
Multiplicity: [1..1]
<br>
Definition: Specifies the type of identifier specified.
<br>
Datatype: “NationalIdentifierTypeCode”
<br>

##### 5.2.8.3.3 countryOfIssue
Multiplicity: [0..1]
<br>
Definition: Country of the issuing authority.
<br>
Datatype: “CountryCode”
<br>

##### 5.2.8.3.4 registrationAuthority
Multiplicity: [0..1]
<br>
Definition: A code specifying the registration authority.
<br>
Datatype: “RegistrationAuthority”
<br><br>

#### 5.2.9 LegalPerson <a id="5.2.9"></a>

##### 5.2.9.1 *LegalPerson structure*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>name</td>
        <td>[1..1]</td>
        <td>LegalPersonName</td>
        <td>C5</td>
        <td></td>
    </tr>
    <tr>
        <td>geographicAddress</td>
        <td>[0..n]</td>
        <td>Address</td>
        <td>C3, C4, C8 </td>
        <td></td>
    </tr>
    <tr>
        <td>customerIdentification</td>
        <td>[0..1]</td>
        <td>Text</td>
        <td>C4</td>
        <td></td>
    </tr>
    <tr>
        <td>nationalIdentification</td>
        <td>[0..1]</td>
        <td>NationalIdentification</td>
        <td>C3, C4, C7, C9, C10, C11</td>
        <td></td>
    </tr>
    <tr>
        <td>countryOfRegistration</td>
        <td>[0..1]</td>
        <td>CountryCode</td>
        <td>C3</td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.9.2 *LegalPerson constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C4 &nbsp; OriginatorInformationLegalPerson**
<br>
If the originator is a LegalPerson either (geographicAddress with an addressType value of ‘GEOG’) and/or nationalIdentification and/or customerIdentifier is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C5 &nbsp; LegalNamePresentLegalPerson**
<br>
At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’ specified in
the element *legalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C7 &nbsp; ValidNationalIdentifierLegalPerson**
<br>
A legal person must have a value for nationalIdentifierType of either ‘RAID’ or ‘MISC’ or ‘LEIX’ or ‘TXID’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C8 &nbsp; ValidAddress**
<br>
There must be at least one occurrence of the element addressLine or (streetName and buildingName and/or buildingNumber).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C9 &nbsp; CompleteNationalIdentifierLegalPerson**
<br>
A LegalPerson must not have a value for countryOfIssue and (must have a value for the element RegistrationAuthority if the value for nationalIdentifierType is not ‘LEIX’ and must not have a value for the element RegistrationAuthority if the value for nationalIdentifierType is ‘LEIX’).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C10 &nbsp; RegistrationAuthority**
<br>
The value used for the applicable element must be present on the GLEIF Registration Authorities List.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C11 &nbsp; ValidLEI**
<br>
A LegalPerson with a nationalIdentifierType of ‘LEIX’ must have a value for the element nationalIdentifier that adheres to the convention as stated in datatype ‘LEIText’.
<br>

##### 5.2.9.3 *LegalPerson elements*

##### 5.2.9.3.1 name
Multiplicity: [1..1]
<br>
Definition: The name of the legal person.
<br>
Component: LegalPersonName
<br>

##### 5.2.9.3.2 geographicAddress
Multiplicity: [0..1]
<br>
Definition: The address of the legal person.
<br>
Component: Address
<br>

##### 5.2.9.3.3 customerIdentifier
Multiplicity: [0..1]
<br>
Definition: The unique identification number applied by the VASP to customer.
<br>
Datatype: “Max50Text”
<br>

##### 5.2.9.3.4 nationalIdentification
Multiplicity: [0..1]
<br>
Definition: A distinct identifier used by governments of countries to uniquely identify a natural or legal person.
<br>
Component: NationalIdentification
<br>

##### 5.2.9.3.5 countryOfRegistration
Multiplicity: [0..1]
<br>
Definition: The country in which the legal person is registered.
<br>
Datatype: “CountryCode”
<br><br>

#### 5.2.10 LegalPersonName <a id="5.2.10"></a>

##### 5.2.10.1 *LegalPersonName structure*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>nameIdentifier</td>
        <td>[1..n]</td>
        <td>LegalPersonNameID</td>
        <td>C5</td>
        <td></td>
    </tr>
    <tr>
        <td>localNameIdentifier</td>
        <td>[0..n]</td>
        <td>LocalLegalPersonNameID</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>phoneticNameIdentifier</td>
        <td>[0..n]</td>
        <td>LocalLegalPersonNameID</td>
        <td></td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.10.2 *LegalPersonName constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C5 &nbsp; LegalNamePresent**
<br>
At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’ specified in
the element *legalPersonNameIdentifierType*.
<br>

##### 5.2.10.3 *LegalPersonName elements*

##### 5.2.10.3.1 nameIdentifier
Multiplicity: [1..n]
<br>
Definition: The name and type of name by which the legal person is known.
<br>
Component: LegalPersonNameID
<br>

##### 5.2.10.3.2 localNameIdentifier
Multiplicity: [0..n]
<br>
Definition: The name and type of name by which the legal person is known using local characters.
<br>
Component: LocalLegalPersonNameID
<br>

##### 5.2.10.3.3 phoneticNameIdentifier
Multiplicity: [0..n]
<br>
Definition: The name and type of name by which the legal person is known using local characters.
<br>
Component: LocalLegalPersonNameID
<br><br>

#### 5.2.11 LegalPersonNameID <a id="5.2.11"></a>

##### 5.2.11.1 *LegalPersonNameID structure*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>legalPersonName</td>
        <td>[1..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>legalPersonNameIdentifierType</td>
        <td>[1..1]</td>
        <td>SingleValueFromList</td>
        <td>C5</td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.11.2 *LegalPersonNameID constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C5 &nbsp; LegalNamePresent**
<br>
At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’ specified in
the element *legalPersonNameIdentifierType*.
<br>

##### 5.2.11.3 *LegalPersonNameID elements*

##### 5.2.11.3.1 legalPersonName
Multiplicity: [1..1]
<br>
Definition: Name by which the legal person is known.
<br>
Datatype: “Max100Text”
<br>

##### 5.2.11.3.2 legalPersonNameIdentifierType
Multiplicity: [1..1]
<br>
Definition: The nature of the name specified.
<br>
Datatype: “LegalPersonNameTypeCode”
<br><br>

#### 5.2.12 LocalLegalPersonNameID <a id="5.2.12"></a>

##### 5.2.12.1 *LocalLegalPersonNameID structure*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>legalPersonName</td>
        <td>[1..1]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>legalPersonNameIdentifierType</td>
        <td>[1..1]</td>
        <td>SingleValueFromList</td>
        <td></td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.12.2 *LocalLegalPersonNameID constraints*
<br>

There are no constraints that apply to the LocalLegalPersonNameID component.

##### 5.2.12.3 *LocalLegalPersonNameID elements*

##### 5.2.12.3.1 legalPersonName
Multiplicity: [1..n]
<br>
Definition: Name by which the legal person is known.
<br>
Datatype: “LocalMax100Text”
<br>

##### 5.2.12.3.2 legalPersonNameIdentifierType
Multiplicity: [1..1]
<br>
Definition: The nature of the name specified.
<br>
Datatype: “LegalPersonNameTypeCode”
<br><br>

#### 5.2.13 IntermediaryVASP <a id="5.2.13"></a>

##### 5.2.13.1 *IntermediaryVASP structure*
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>intermediaryVASP</td>
        <td>[1..1]</td>
        <td>Person</td>
        <td>C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11</td>
        <td></td>
    </tr>
    <tr>
        <td>sequence</td>
        <td>[1..1]</td>
        <td>Number</td>
        <td>C12</td>
        <td></td>
    </tr>
</table>
<br>

##### 5.2.13.2 *IntermediaryVASP constraints*
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C1 &nbsp; OriginatorInformationNaturalPerson**
<br>
If the originator is a NaturalPerson either (geographicAddress with an addressType value of ‘GEOG’ or ‘HOME’ or ‘BIZZ’) and/or customerIdentifier and/or nationalIdentification and/or dateAndPlaceOfBirth is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C2 &nbsp; DateInPast**
<br>
If dateOfBirth is specified, the date specified must be a historic date (i.e. a date prior to the current date).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C4 &nbsp; OriginatorInformationLegalPerson**
<br>
If the originator is a LegalPerson either (geographicAddress with an addressType value of ‘GEOG’) and/or customerIdentifier and/or nationalIdentification is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C5 &nbsp; LegalNamePresentLegalPerson**
<br>
At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’ specified in the element *legalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C6 &nbsp; LegalNamePresentNaturalPerson**
<br>
At least one occurrence of naturalPersonNameID must have the value ‘LEGL’ specified in the element *naturalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C7 &nbsp; ValidNationalIdentifierLegalPerson**
<br>
A legal person must have a value for nationalIdentifierType of either ‘RAID’ or ‘MISC’ or ‘LEIX’ or ‘TXID’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C8 &nbsp; ValidAddress**
<br>
There must be at least one occurrence of the element addressLine or (streetName and buildingName and/or buildingNumber).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C9 &nbsp; CompleteNationalIdentifierLegalPerson**
<br>
A LegalPerson must not have a value for countryOfIssue and (must have a value for the element RegistrationAuthority if the value for nationalIdentifierType is not ‘LEIX’ and must not have a value for the element RegistrationAuthority if the value for nationalIdentifierType is ‘LEIX’).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C10 &nbsp; registrationAuthority**
<br>
The value used for the applicable element must be present on the GLEIF Registration Authorities List.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C11 &nbsp; validLEI**
<br>
A LegalPerson with a nationalIdentifierType of ‘LEIX’ must have a value for the element nationalIdentifier that adheres to the convention as stated in datatype ‘LEIText’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C12 &nbsp; sequentialIntegrity**
<br>
Occurrences of this component must have a corresponding sequence value starting from 0 and remain uninterrupted through to the final instance.
<br>

##### 5.2.13.3 *IntermediaryVASP elements*

##### 5.2.13.3.1 intermediaryVASP
Multiplicity: [1..1]
<br>
Definition: the VASP in a serial chain that receives and retransmits a VA transfer on behalf of the originating VASP and the beneficiary VASP, or another intermediary VASP.
<br>
Component: Person
<br>

##### 5.2.13.3.2 sequence
Multiplicity: [1..1]
<br>
Definition: the sequence in a serial chain at which the corresponding intermediary VASP participates in the transfer.
<br>
Datatype: “Number”
<br><br>

### 5.3 Datatypes <a id="5.3"></a>
<br>

#### 5.3.1 “Max100Text” <a id="5.3.1"></a>

Definition: A character string that is restricted to a maximum length of 100 characters, limited to Latin characters and numbers.

Type: Text

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Minimum Length: 1
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Maximum Length: 100

Regex:
`^[a-zA-Z0-9' ]{1,100}$`
<br><br>

#### 5.3.2 “LocalMax100Text” <a id="5.3.2"></a>

Definition: A character string that is restricted to a maximum length of 100 characters.

Type: Text

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Minimum Length: 1
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Maximum Length: 100

Regex:
`^.{1,100}$`
<br><br>

#### 5.3.3 “Max50Text” <a id="5.3.3"></a>

Definition: A character string that is restricted to a maximum length of 50 characters, limited to Latin characters and numbers.

Type: Text

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Minimum Length: 1
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Maximum Length: 50

Regex:
`^[a-zA-Z0-9' ]{1,50}$`
<br><br>

#### 5.3.4 “Max70Text” <a id="5.3.4"></a>

Definition: A character string that is restricted to a maximum length of 70 characters, limited to Latin characters and numbers.

Type: Text

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Minimum Length: 1
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Maximum Length: 70

Regex:
`^[a-zA-Z0-9' ]{1,70}$`
<br><br>

#### 5.3.5 “Max35Text” <a id="5.3.5"></a>

Definition: A character string that is restricted to a maximum length of 35 characters, limited to Latin characters and numbers.

Type: Text

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Minimum Length: 1
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Maximum Length: 35

Regex:
`^[a-zA-Z0-9' ]{1,35}$`
<br><br>

#### 5.3.6 “Max16Text” <a id="5.3.6"></a>

Definition: A character string that is restricted to a maximum length of 16 characters, limited
to Latin characters and numbers.

Type: Text

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Minimum Length: 1
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Maximum Length: 16

Regex:
`^[a-zA-Z0-9' ]{1,16}$`
<br><br>

#### 5.3.7 “LEIText” <a id="5.3.7"></a>

Definition: a 20-character, alpha-numeric code.

Type: Text

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Minimum Length: 20
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Maximum Length: 20

Regex:
`^[0-9A-Z]{18}[0-9]{2}$`
<br><br>

#### 5.3.8 “NaturalPersonNameTypeCode” <a id="5.3.8"></a>

Definition: A single value corresponding to the nature of name being adopted.
<table>
    <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>ALIA</td>
        <td>Alias name</td>
        <td>A name other than the legal name by which a natural person is also known.</td>
    </tr>
    <tr>
        <td>BIRT</td>
        <td>Name at birth</td>
        <td>The name given to a natural person at birth.</td>
    </tr>
    <tr>
        <td>MAID</td>
        <td>Maiden name</td>
        <td>The original name of a natural person who has changed their name after marriage</td>
    </tr>
    <tr>
        <td>LEGL</td>
        <td>Legal name</td>
        <td>The name that identifies a natural person for legal, official or administrative purposes.</td>
    </tr>
    <tr>
        <td>MISC</td>
        <td>Unspecified</td>
        <td>A name by which a natural person may be known but which cannot otherwise be categorized or the category of which the sender is unable to determine.</td>
    </tr>
</table>
<br><br>

#### 5.3.9 “LegalPersonNameTypeCode” <a id="5.3.9"></a>

Definition: A single value corresponding to the nature of name being specified for the legal person.
<table>
    <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>LEGL</td>
        <td>Legal name</td>
        <td>Official name under which an organisation is registered.</td>
    </tr>
    <tr>
        <td>SHRT</td>
        <td>Short name</td>
        <td>Specifies the short name of the organisation.</td>
    </tr>
    <tr>
        <td>TRAD</td>
        <td>Trading name</td>
        <td>Name used by a business for commercial purposes, although its registered legal name, used for contracts and other formal situations, may be another.</td>
    </tr>
</table>
<br><br>

#### 5.3.10 “Date” <a id="5.3.10"></a>

Definition: A point in time, represented as a day within the calendar year. Compliant with ISO 8601.

Type: Text

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; YYYY-MM-DD

Regex:
`^([0-9]{4})-([0-9]{2})-([0-9]{2})$`
<br><br>

#### 5.3.11 “AddressTypeCode” <a id="5.3.11"></a>

Definition: Identifies the nature of the address.
<table>
    <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>HOME</td>
        <td>Residential</td>
        <td>Address is the home address.</td>
    </tr>
    <tr>
        <td>BIZZ</td>
        <td>Business</td>
        <td>Address is the business address.</td>
    </tr>
    <tr>
        <td>GEOG<a href='#ref-7'><sup>7</sup></a></td>
        <td>Geographic</td>
        <td>Address is the unspecified physical (geographical) address suitable for identification of the natural or legal person.</td>
    </tr>
</table>
<br><br>

#### 5.3.12 “CountryCode” <a id="5.3.12"></a>

Definition: two alphabetic characters representing an ISO-3166 Alpha-2 country, including the code ‘XX’ to represent ‘an indicator for unknown States, other entities or organisations’[<sup>8</sup>](#ref-8).

Type: Text

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; AA

Regex:
`^[A-Z]{2}$`
<br><br>

#### 5.3.13 “RegistrationAuthority”[<sup>9</sup>](#ref-9) <a id="5.3.13"></a>

Definition: 8-character code representing a legal entity registration authority.

Type: Text

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; RA000099

Regex:
`^RA([0-9]{6})$`
<br><br>

#### 5.3.14 “NationalIdentifierTypeCode”[<sup>10</sup>](#ref-10) <a id="5.3.14"></a>

Definition: Identifies the national identification type.
<table>
    <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>ARNU</td>
        <td>Alien registration number</td>
        <td>Number assigned by a government agency to identify foreign nationals.</td>
    </tr>
    <tr>
        <td>CCPT</td>
        <td>Passport number</td>
        <td>Number assigned by a passport authority.</td>
    </tr>
    <tr>
        <td>RAID</td>
        <td>Registration authority identifier</td>
        <td>Identifier of a legal entity as maintained by a registration authority.</td>
    </tr>
    <tr>
        <td>DRLC</td>
        <td>Driver license number</td>
        <td>Number assigned to a driver's license.</td>
    </tr>
    <tr>
        <td>FIIN</td>
        <td>Foreign investment identity number</td>
        <td>Number assigned to a foreign investor (other than the alien number).</td>
    </tr>
    <tr>
        <td>TXID</td>
        <td>Tax identification number</td>
        <td>Number assigned by a tax authority to an entity.</td>
    </tr>
    <tr>
        <td>SOCS</td>
        <td>Social security number</td>
        <td>Number assigned by a social security agency.</td>
    </tr>
    <tr>
        <td>IDCD</td>
        <td>Identity card number</td>
        <td>Number assigned by a national authority to an identity card.</td>
    </tr>
    <tr>
        <td>LEIX</td>
        <td>Legal Entity Identifier</td>
        <td>Legal Entity Identifier (LEI) assigned in accordance with ISO 17442<a href="#ref-11"><sup>11</sup></a>.</td>
    </tr>
    <tr>
        <td>MISC</td>
        <td>Unspecified</td>
        <td>A national identifier which may be known but which cannot otherwise be categorized or the category of which the sender is unable to determine.</td>
    </tr>
</table>
<br><br>

#### 5.3.15 “Number” <a id="5.3.15"></a>

Definition: Number represented as an integer.

Format:
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; totalDigits: 18
<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; fractionDigits: 0
<br><br>

#### 5.3.16 “TransliterationMethodCode”[<sup>12</sup>](#ref-12) <a id="5.3.16"></a>

Definition: Identifies the national script from which transliteration to Latin script is applied.
<table>
    <tr>
        <th>Code</th>
        <th>Name</th>
        <th>Description</th>
    </tr>
    <tr>
        <td>arab</td>
        <td>Arabic (Arabic language)</td>
        <td>ISO 233-2:1993</td>
    </tr>
    <tr>
        <td>aran</td>
        <td>Arabic (Persian language)</td>
        <td>ISO 233-3:1999</td>
    </tr>
    <tr>
        <td>armn</td>
        <td>Armenian</td>
        <td>ISO 9985:1996</td>
    </tr>
    <tr>
        <td>cyrl</td>
        <td>Cyrillic</td>
        <td>ISO 9:1995</td>
    </tr>
    <tr>
        <td>deva</td>
        <td>Devanagari & related Indic</td>
        <td>ISO 15919:2001</td>
    </tr>
    <tr>
        <td>geor</td>
        <td>Georgian</td>
        <td>ISO 9984:1996</td>
    </tr>
    <tr>
        <td>grek</td>
        <td>Greek</td>
        <td>ISO 843:1997</td>
    </tr>
    <tr>
        <td>hani</td>
        <td>Han (Hanzi, Kanji, Hanja)</td>
        <td>ISO 7098:2015</td>
    </tr>
    <tr>
        <td>hebr</td>
        <td>Hebrew</td>
        <td>ISO 259-2:1994</td>
    </tr>
    <tr>
        <td>kana</td>
        <td>Kana</td>
        <td>ISO 3602:1989</td>
    </tr>
    <tr>
        <td>kore</td>
        <td>Korean</td>
        <td>Revised Romanization of Korean</td>
    </tr>
    <tr>
        <td>thai</td>
        <td>Thai</td>
        <td>ISO 11940-2:2007</td>
    </tr>
    <tr>
        <td>othr</td>
        <td>Script other than those listed above</td>
        <td>Unspecified</td>
    </tr>
</table>
<br><br><br>

---
<br>

## 6. Data model definition <a id="6"></a>
<br>

### 6.1 Context for the data model <a id="6.1"></a>
<br>

This section describes the entities identified as being in scope as set out in Section 2 (Background & Rationale). It includes the structures and constraints applicable to the following entities:
- Originator
- Beneficiary
- Originating VASP
- Beneficiary VASP
- Transfer path
- Payload metadata.

Figure 4, IVMS data model taxonomy (2) represents the objects within the IVMS data model taxonomy that are documented in this section. 

<figure>
  <img src="context-of-data-model.png" alt="Context of data model">
  <figcaption>Figure 4 IVMS data model taxonomy (2) data entity through to element</figcaption>
</figure>
<br>

### 6.2 Originator <a id="6.2"></a>
<br>

The originator is defined in Section 1.1 as the account holder who allows the VA transfer from that account or, where there is no account, the natural or legal person that places the order with the originating VASP to perform the VA transfer.
<br>

#### 6.2.1 Originator structure <a id="6.2.1"></a>
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>originatorPersons</td>
        <td>[1..n]</td>
        <td>Person</td>
        <td>C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11</td>
        <td></td>
    </tr>
    <tr>
        <td>accountNumber</td>
        <td>[0..n]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
</table>
<br>

#### 6.2.2 Originator constraints <a id="6.2.2"></a>
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C1 &nbsp; OriginatorInformationNaturalPerson**
<br>
If the originator is a NaturalPerson either (geographicAddress with an addressType value of ‘GEOG’ or ‘HOME’ or ‘BIZZ’) and/or customerIdentifier and/or nationalIdentification and/or dateAndPlaceOfBirth is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C2 &nbsp; DateInPast**
<br>
If dateOfBirth is specified, the date specified must be a historic date (i.e. a date prior to the current date).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C4 &nbsp; OriginatorInformationLegalPerson**
<br>
If the originator is a LegalPerson either (geographicAddress with an addressType value of ‘GEOG’) and/or customerIdentifier and/or nationalIdentification is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C5 &nbsp; LegalNamePresentLegalPerson**
<br>
At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’ specified in the element *legalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C6 &nbsp; LegalNamePresentNaturalPerson**
<br>
At least one occurrence of naturalPersonNameID must have the value ‘LEGL’ specified in the element *naturalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C7 &nbsp; ValidNationalIdentifierLegalPerson**
<br>
A legal person must have a value for nationalIdentifierType of either ‘RAID’ or ‘MISC’ or ‘LEIX’ or ‘TXID’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C8 &nbsp; ValidAddress**
<br>
There must be at least one occurrence of the element addressLine or (streetName and buildingName and/or buildingNumber).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C9 &nbsp; CompleteNationalIdentifierLegalPerson**
<br>
A LegalPerson must not have a value for countryOfIssue and (must have a value for the element RegistrationAuthority if the value for nationalIdentifierType is not ‘LEIX’ and must not have a value for the element RegistrationAuthority if the value for nationalIdentifierType is ‘LEIX’).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C10 &nbsp; registrationAuthority**
<br>
The value used for the applicable element must be present on the GLEIF Registration Authorities List.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C11 &nbsp; validLEI**
<br>
A LegalPerson with a nationalIdentifierType of ‘LEIX’ must have a value for the element nationalIdentifier that adheres to the convention as stated in datatype ‘LEIText’.
<br>

#### 6.2.3 Originator elements <a id="6.2.3"></a>

##### 6.2.3.1 originatorPersons
Multiplicity: [1..n]
<br>
Definition: the account holder who allows the VA transfer from that account or, where there is no account, the natural or legal person that places the order with the originating VASP to perform the VA transfer.
<br>
Component: Person
<br>

##### 6.2.3.2 accountNumber
Multiplicity: [0..1]
<br>
Definition: Identifier of an account that is used to process the transaction. The value for this element is case-sensitive.
<br>
Datatype: “Max100Text”
<br><br>

### 6.3 Beneficiary <a id="6.3"></a>
<br>

The beneficiary is defined in Section 1.1 as the natural or legal person or legal arrangement who is identified by the originator as the receiver of the requested VA transfer.

#### 6.3.1 Beneficiary structure <a id="6.3.1"></a>
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>beneficiaryPerson</td>
        <td>[1..n]</td>
        <td>Person</td>
        <td>C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11</td>
        <td></td>
    </tr>
    <tr>
        <td>accountNumber</td>
        <td>[0..n]</td>
        <td>Text</td>
        <td></td>
        <td></td>
    </tr>
</table>
<br>

#### 6.3.2 Beneficiary constraints <a id="6.3.2"></a>
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C1 &nbsp; OriginatorInformationNaturalPerson**
<br>
If the originator is a NaturalPerson either (geographicAddress with an addressType value of ‘GEOG’ or ‘HOME’ or ‘BIZZ’) and/or customerIdentifier and/or nationalIdentification and/or dateAndPlaceOfBirth is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C2 &nbsp; DateInPast**
<br>
If dateOfBirth is specified, the date specified must be a historic date (i.e. a date prior to the current date).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C4 &nbsp; OriginatorInformationLegalPerson**
<br>
If the originator is a LegalPerson either (geographicAddress with an addressType value of ‘GEOG’) and/or customerIdentifier and/or nationalIdentification is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C5 &nbsp; LegalNamePresentLegalPerson**
<br>
At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’ specified in the element *legalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C6 &nbsp; LegalNamePresentNaturalPerson**
<br>
At least one occurrence of naturalPersonNameID must have the value ‘LEGL’ specified in the element *naturalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C7 &nbsp; ValidNationalIdentifierLegalPerson**
<br>
A legal person must have a value for nationalIdentifierType of either ‘RAID’ or ‘MISC’ or ‘LEIX’ or ‘TXID’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C8 &nbsp; ValidAddress**
<br>
There must be at least one occurrence of the element addressLine or (streetName and buildingName and/or buildingNumber).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C9 &nbsp; CompleteNationalIdentifierLegalPerson**
<br>
A LegalPerson must not have a value for countryOfIssue and (must have a value for the element RegistrationAuthority if the value for nationalIdentifierType is not ‘LEIX’ and must not have a value for the element RegistrationAuthority if the value for nationalIdentifierType is ‘LEIX’).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C10 &nbsp; registrationAuthority**
<br>
The value used for the applicable element must be present on the GLEIF Registration Authorities List.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C11 &nbsp; validLEI**
<br>
A LegalPerson with a nationalIdentifierType of ‘LEIX’ must have a value for the element nationalIdentifier that adheres to the convention as stated in datatype ‘LEIText’.
<br>

#### 6.3.3 Beneficiary elements <a id="6.3.3"></a>

##### 6.3.3.1 beneficiaryPerson
Multiplicity: [1..n]
<br>
Definition: the natural or legal person or legal arrangement who is identified by the originator as the receiver of the requested VA transfer.
<br>
Component: Person
<br>

##### 6.3.3.2 accountNumber
Multiplicity: [0..1]
<br>
Definition: Identifier of an account that is used to process the transaction. The value for this element is case-sensitive.
<br>
Datatype: “Max100Text”
<br><br>

### 6.4 OriginatingVASP <a id="6.4"></a>
<br>

The originating VASP is defined in Section 1.1 as the VASP which initiates the VA transfer, and transfers the VA upon receiving the request for a VA transfer on behalf of the originator

#### 6.4.1 OriginatingVASP structure <a id="6.4.1"></a>
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>originatingVASP</td>
        <td>[0..1]</td>
        <td>Person</td>
        <td>C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11</td>
        <td></td>
    </tr>
</table>
<br>

#### 6.4.2 OriginatingVASP constraints <a id="6.4.2"></a>
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C1 &nbsp; OriginatorInformationNaturalPerson**
<br>
If the originator is a NaturalPerson either (geographicAddress with an addressType value of ‘GEOG’ or ‘HOME’ or ‘BIZZ’) and/or customerIdentifier and/or nationalIdentification and/or dateAndPlaceOfBirth is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C2 &nbsp; DateInPast**
<br>
If dateOfBirth is specified, the date specified must be a historic date (i.e. a date prior to the current date).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C4 &nbsp; OriginatorInformationLegalPerson**
<br>
If the originator is a LegalPerson either (geographicAddress with an addressType value of ‘GEOG’) and/or customerIdentifier and/or nationalIdentification is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C5 &nbsp; LegalNamePresentLegalPerson**
<br>
At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’ specified in the element *legalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C6 &nbsp; LegalNamePresentNaturalPerson**
<br>
At least one occurrence of naturalPersonNameID must have the value ‘LEGL’ specified in the element *naturalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C7 &nbsp; ValidNationalIdentifierLegalPerson**
<br>
A legal person must have a value for nationalIdentifierType of either ‘RAID’ or ‘MISC’ or ‘LEIX’ or ‘TXID’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C8 &nbsp; ValidAddress**
<br>
There must be at least one occurrence of the element addressLine or (streetName and buildingName and/or buildingNumber).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C9 &nbsp; CompleteNationalIdentifierLegalPerson**
<br>
A LegalPerson must not have a value for countryOfIssue and (must have a value for the element RegistrationAuthority if the value for nationalIdentifierType is not ‘LEIX’ and must not have a value for the element RegistrationAuthority if the value for nationalIdentifierType is ‘LEIX’).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C10 &nbsp; registrationAuthority**
<br>
The value used for the applicable element must be present on the GLEIF Registration Authorities List.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C11 &nbsp; validLEI**
<br>
A LegalPerson with a nationalIdentifierType of ‘LEIX’ must have a value for the element nationalIdentifier that adheres to the convention as stated in datatype ‘LEIText’.
<br>

#### 6.4.3 OriginatingVASP elements <a id="6.4.3"></a>

##### 6.4.3.1 originatingVASP
Multiplicity: [0..1]
<br>
Definition: refers to the VASP which initiates the VA transfer, and transfers the VA upon receiving the request for a VA transfer on behalf of the originator.
<br>
Component: Person
<br><br>

### 6.5 BeneficiaryVASP <a id="6.5"></a>
<br>

The beneficiary is defined in Section 1.1 as the VASP which receives the transfer of a virtual asset from the originating VASP directly or through an intermediary VASP and makes the funds available to the beneficiary.

#### 6.5.1 BeneficiaryVASP structure <a id="6.5.1"></a>
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>beneficiaryVASP</td>
        <td>[0..1]</td>
        <td>Person</td>
        <td>C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11</td>
        <td></td>
    </tr>
</table>
<br>

#### 6.5.2 BeneficiaryVASP constraints <a id="6.5.2"></a>
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C1 &nbsp; OriginatorInformationNaturalPerson**
<br>
If the originator is a NaturalPerson either (geographicAddress with an addressType value of ‘GEOG’ or ‘HOME’ or ‘BIZZ’) and/or customerIdentifier and/or nationalIdentification and/or dateAndPlaceOfBirth is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C2 &nbsp; DateInPast**
<br>
If dateOfBirth is specified, the date specified must be a historic date (i.e. a date prior to the current date).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C4 &nbsp; OriginatorInformationLegalPerson**
<br>
If the originator is a LegalPerson either (geographicAddress with an addressType value of ‘GEOG’) and/or customerIdentifier and/or nationalIdentification is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C5 &nbsp; LegalNamePresentLegalPerson**
<br>
At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’ specified in the element *legalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C6 &nbsp; LegalNamePresentNaturalPerson**
<br>
At least one occurrence of naturalPersonNameID must have the value ‘LEGL’ specified in the element *naturalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C7 &nbsp; ValidNationalIdentifierLegalPerson**
<br>
A legal person must have a value for nationalIdentifierType of either ‘RAID’ or ‘MISC’ or ‘LEIX’ or ‘TXID’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C8 &nbsp; ValidAddress**
<br>
There must be at least one occurrence of the element addressLine or (streetName and buildingName and/or buildingNumber).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C9 &nbsp; CompleteNationalIdentifierLegalPerson**
<br>
A LegalPerson must not have a value for countryOfIssue and (must have a value for the element RegistrationAuthority if the value for nationalIdentifierType is not ‘LEIX’ and must not have a value for the element RegistrationAuthority if the value for nationalIdentifierType is ‘LEIX’).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C10 &nbsp; registrationAuthority**
<br>
The value used for the applicable element must be present on the GLEIF Registration Authorities List.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C11 &nbsp; validLEI**
<br>
A LegalPerson with a nationalIdentifierType of ‘LEIX’ must have a value for the element nationalIdentifier that adheres to the convention as stated in datatype ‘LEIText’.
<br>

#### 6.5.3 BeneficiaryVASP elements <a id="6.5.3"></a>

##### 6.5.3.1 beneficiaryVASP
Multiplicity: [0..1]
<br>
Definition: the VASP which receives the transfer of a virtual asset from the originating VASP directly or through an intermediary VASP and makes the funds available to the beneficiary.
<br>
Component: Person
<br><br>

### 6.6 TransferPath <a id="6.6"></a>
<br>

The transfer path refers to the intermediary VASP(s) participating in a serial chain that receive(s) and retransmit(s) a VA transfer on behalf of the originating VASP and the beneficiary VASP, or another intermediary VASP, together with their corresponding sequence number.

#### 6.6.1 TransferPath structure <a id="6.6.1"></a>
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>transferPath</td>
        <td>[0..n]</td>
        <td>IntermediaryVASP</td>
        <td>C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11, C12</td>
        <td></td>
    </tr>
</table>
<br>

#### 6.6.2 TransferPath constraints <a id="6.6.2"></a>
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C1 &nbsp; OriginatorInformationNaturalPerson**
<br>
If the originator is a NaturalPerson either (geographicAddress with an addressType value of ‘GEOG’ or ‘HOME’ or ‘BIZZ’) and/or customerIdentifier and/or nationalIdentification and/or dateAndPlaceOfBirth is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C2 &nbsp; DateInPast**
<br>
If dateOfBirth is specified, the date specified must be a historic date (i.e. a date prior to the current date).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C3 &nbsp; Country**
<br>
The value used for the field country must be present on the ISO-3166-1 alpha-2 codes or the value XX.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C4 &nbsp; OriginatorInformationLegalPerson**
<br>
If the originator is a LegalPerson either (geographicAddress with an addressType value of ‘GEOG’) and/or customerIdentifier and/or nationalIdentification is required.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C5 &nbsp; LegalNamePresentLegalPerson**
<br>
At least one occurrence of legalPersonNameIdentifier must have the value ‘LEGL’ specified in the element *legalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C6 &nbsp; LegalNamePresentNaturalPerson**
<br>
At least one occurrence of naturalPersonNameID must have the value ‘LEGL’ specified in the element *naturalPersonNameIdentifierType*.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C7 &nbsp; ValidNationalIdentifierLegalPerson**
<br>
A legal person must have a value for nationalIdentifierType of either ‘RAID’ or ‘MISC’ or ‘LEIX’ or ‘TXID’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C8 &nbsp; ValidAddress**
<br>
There must be at least one occurrence of the element addressLine or (streetName and buildingName and/or buildingNumber).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C9 &nbsp; CompleteNationalIdentifierLegalPerson**
<br>
A LegalPerson must not have a value for countryOfIssue and (must have a value for the element RegistrationAuthority if the value for nationalIdentifierType is not ‘LEIX’ and must not have a value for the element RegistrationAuthority if the value for nationalIdentifierType is ‘LEIX’).
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C10 &nbsp; registrationAuthority**
<br>
The value used for the applicable element must be present on the GLEIF Registration Authorities List.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C11 &nbsp; validLEI**
<br>
A LegalPerson with a nationalIdentifierType of ‘LEIX’ must have a value for the element nationalIdentifier that adheres to the convention as stated in datatype ‘LEIText’.
<br>

&nbsp;&nbsp;&nbsp;&nbsp; **C12 &nbsp; sequentialIntegrity**
<br>
Occurrences of this component must have a corresponding sequence value starting from 0 and remain uninterrupted through to the final instance.
<br>

#### 6.6.3 TransferPath elements <a id="6.6.3"></a>

##### 6.6.3.1 transferPath
Multiplicity: [0..n]
<br>
Definition: the intermediary VASP(s) participating in a serial chain that receive and retransmit a VA transfer on behalf of the originating VASP and the beneficiary VASP, or another intermediary VASP, together with their corresponding sequence number.
<br>
Component: IntermediaryVASP
<br><br>

### 6.7 PayloadMetadata <a id="6.7"></a>
<br>

Data describing the contents of the payload.

#### 6.7.1 PayloadMetadata structure <a id="6.7.1"></a>
<table>
    <tr>
        <th>Element</th>
        <th>Multiplicity</th>
        <th>Type</th>
        <th>Constraint</th>
        <th>OR</th>
    </tr>
    <tr>
        <td>transliterationMethod</td>
        <td>[0..n]</td>
        <td>SingleValueFromList</td>
        <td></td>
        <td></td>
    </tr>
</table>
<br>

#### 6.7.2 PayloadMetadata constraints <a id="6.7.2"></a>
<br>

There are no constraints that apply to the PayloadMetadata entity.

#### 6.7.3 PayloadMetadata elements <a id="6.7.3"></a>

##### 6.7.3.1 transliterationMethod
Multiplicity: [0..n]
<br>
Definition: the method used to map from a national system of writing to Latin script.
<br>
Datatype: “TransliterationMethodCode”
<br><br><br>

---
<br>

## 7. Handling of multiple character sets <a id="7"></a>
<br>

### 7.1 Character set and character encoding <a id="7.1"></a>
<br>

Data shall be submitted using UTF-8 character encoding. Unless otherwise specified, data shall be represented in Latin script (i.e. A to Z, a to z) and Arabic numerals (i.e. 1234567890). 

### 7.2 Transformation of national language <a id="7.2"></a>
<br>

Where data is in a national language that does not use Latin script, it must be either:
- Transliterated into Latin characters; or
- Translated into a language to which it may be more commonly known to the international community[<sup>13</sup>](#ref-13).

Data may also be transmitted using national language where a suitable element exists (i.e. those elements prefixed ‘local’). 

### 7.3 Transliteration standards <a id="7.3"></a>
<br>

The following scripts shall be transliterated into Latin characters using the corresponding standards set out below: 
<table>
    <tr>
        <th>Script</th>
        <th>Standard</th>
    </tr>
    <tr>
        <td>Arabic (Arabic language)</td>
        <td>ISO 233-2:1993</td>
    </tr>
    <tr>
        <td>Arabic (Persian language)</td>
        <td>ISO 233-3:1999</td>
    </tr>
    <tr>
        <td>Armenian</td>
        <td>ISO 9985:1996</td>
    </tr>
    <tr>
        <td>Cyrillic</td>
        <td>ISO 9:1995</td>
    </tr>
    <tr>
        <td>Devanagari & related Indic</td>
        <td>ISO 15919:2001</td>
    </tr>
    <tr>
        <td>Han (Hanzi, Kanji, Hanja)</td>
        <td>ISO 7098:2015</td>
    </tr>
    <tr>
        <td>Hebrew</td>
        <td>ISO 259-2:1994</td>
    </tr>
    <tr>
        <td>Georgian</td>
        <td>ISO 9984:1996</td>
    </tr>
    <tr>
        <td>Greek</td>
        <td>ISO 843:1997</td>
    </tr>
    <tr>
        <td>Kana</td>
        <td>ISO 3602:1989</td>
    </tr>
    <tr>
        <td>Korean</td>
        <td>Revised Romanization of Korean<a href='#ref-14'><sup>14</sup></a></td>
    </tr>
    <tr>
        <td>Thai</td>
        <td>ISO 11940-2:2007</td>
    </tr>
</table>
<br><br><br>

---
<br>

## 8. Business examples <a id="8"></a>
<br>

### 8.1 Example 1 <a id="8.1"></a>
<br>

VASP A, on behalf of originator Alice Smith, transfers 1BTC to Bob Barnes, who holds a custodial wallet with VASP B.

*Originator*
<br>
Dr Alice Smith is an American national residing at 1 Weathering Views, 24 Potential Street,
91765, Walnut, California USA. She was born on the 25<sup>th</sup> of February 1990. 

*Beneficiary*
<br>
Mr. Bob Barnes is a Canadian national residing at 42 St Johns Close, Pershing Wells, PW3 9NZ
London, UK. He was born on the 14<sup>th</sup> of January 1963 in London, Ontario.

*Originating VASP*
<br>
VASP A is a financial institution providing covered VA activities. For the purposes of sharing identification information per Recommendation 16, VASP A uses the geographic address of the customer. The VASP has also been assigned an LEI, which is 3M5E1GQKGL17HI8CPN20.

**Business Data**
<br>
The IVMS101 payload between VASP A and VASP B:
<table>
    <tr>
        <th>Element</th>
        <th>Content</th>
    </tr>
    <tr>
        <td><h6>Originator</h6></td>
    </tr>
    <tr>
        <td>originatorPersons</td>
        <td></td>
    </tr>
    <tr>
        <td>naturalPerson</td>
        <td></td>
    </tr>
    <tr>
        <td>name*</td>
        <td></td>
    </tr>
    <tr>
        <td>nameIdentifier*</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;primaryIdentifier</td>
        <td>Smith</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;secondaryIdentifier</td>
        <td>Dr Alice</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;nameIdentifierType</td>
        <td>LEGL</td>
    </tr>
    <tr>
        <td>geographicAddress<a href='#ref-15'><sup>15</sup></a></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;addressType</td>
        <td>GEOG</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;streetName</td>
        <td>Potential Street</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;buildingNumber</td>
        <td>24</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;buildingName</td>
        <td>Weathering Views</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;postcode</td>
        <td>91765</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;townName</td>
        <td>Walnut</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;countrySubDivision</td>
        <td>California</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;country</td>
        <td>US</td>
    </tr>
    <tr>
        <td>customerIdentification</td>
        <td>1002390</td>
    </tr>
    <tr>
        <td>accountNumber</td>
        <td>10023909</td>
    </tr>
    <tr>
        <td><h6>Beneficiary</h6></td>
    </tr>
    <tr>
        <td>beneficiaryPersons</td>
        <td></td>
    </tr>
    <tr>
        <td>naturalPerson</td>
        <td></td>
    </tr>
    <tr>
        <td>name*</td>
        <td></td>
    </tr>
    <tr>
        <td>nameIdentifier*</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;countrySubDivision</td>
        <td>Barnes</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;secondaryIdentifier</td>
        <td>Robert</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;nameIdentifierType</td>
        <td>LEGL</td>
    </tr>
    <tr>
        <td>accountNumber</td>
        <td>1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2</td>
    </tr>
    <tr>
        <td><h6>OriginatingVASP</h6></td>
    </tr>
    <tr>
        <td>originatingVASP</td>
        <td></td>
    </tr>
    <tr>
        <td>name*</td>
        <td></td>
    </tr>
    <tr>
        <td>nameIdentifier*</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;legalPersonName</td>
        <td>VASP A</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;legalPersonNameIdentifierType</td>
        <td>LEGL</td>
    </tr>
    <tr>
        <td>nationalIdentification</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;legalPersonNames</td>
        <td>3M5E1GQKGL17HI8CPN20</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;nationalIdentifierType</td>
        <td>LEIX</td>
    </tr>
</table>
<br>

### 8.2 Example 2 <a id="8.2"></a>
<br>

VASP C, on behalf of originator 吴信利 (Xinli Wu), transfers 1 ETH to ABC Limited (trading as CBA Trading), which holds a custodial wallet with VASP D. VASP C routes ETH transfers through the intermediary VASP E.

*Originator*
<br>
吴信利 (Xinli Wu) is a Chinese national residing in Dar es Salaam, Tanzania. He operates as a sole trader, contracting for a multi-national construction firm in providing civil engineering expertise. Xinli Wu is registered as such under his own name by the Business Registrations and Licensing Agency (BRELA), registration number 446005. Xinli Wu uses VASP C for safekeeping, exchanging and remittances of virtual assets used in a business capacity.

*Beneficiary*
<br>
ABC Limited also goes by the registered trading name of CBA Trading in Santiago, Chile. The beneficiary holds a managed wallet with VASP D.

*Originating VASP*
<br>
VASP C serves as an exchange for virtual assets, custodial wallet provider and provides a means to send and receive virtual assets. Each managed wallet is assigned a unique identifier. For the purposes of sharing identification information per Recommendation 16, VASP C uses the national identifier of the customer.

Transfer Path
<br>
VASP C routes ETH transfers through the intermediary VASP E

**Business Data**
<br>
The IVMS101 payload between VASP C and VASP D:
<table>
    <tr>
        <th>Element</th>
        <th>Content</th>
    </tr>
    <tr>
        <td><h6>Originator</h6></td>
    </tr>
    <tr>
        <td>originatorPersons</td>
        <td></td>
    </tr>
    <tr>
        <td>naturalPerson</td>
        <td></td>
    </tr>
    <tr>
        <td>name*</td>
        <td></td>
    </tr>
    <tr>
        <td>nameIdentifier*</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;primaryIdentifier</td>
        <td>Wu</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;secondaryIdentifier</td>
        <td>Xinli</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;nameIdentifierType</td>
        <td>LEGL</td>
    </tr>
    <tr>
        <td>localNameIdentifier</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;primaryIdentifier</td>
        <td>吴</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;secondaryIdentifier</td>
        <td>信利</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;nameIdentifierType</td>
        <td>LEGL</td>
    </tr>
    <tr>
        <td>nationalIdentification<a href='#ref-16'><sup>16</sup></a></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;nationalIdentifier</td>
        <td>446005</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;nationalIdentifierType</td>
        <td>RAID</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;registrationAuthority</td>
        <td>RA000553</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;countryOfResidence</td>
        <td>TZ</td>
    </tr>
    <tr>
        <td><h6>Beneficiary</h6></td>
    </tr>
    <tr>
        <td>beneficiaryPersons</td>
        <td></td>
    </tr>
    <tr>
        <td>legalPerson</td>
        <td></td>
    </tr>
    <tr>
        <td>name*</td>
        <td></td>
    </tr>
    <tr>
        <td>nameIdentifier*</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;legalPersonName</td>
        <td>ABC Limited</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;legalPersonNameIdentifierType</td>
        <td>LEGL</td>
    </tr>
    <tr>
        <td>nameIdentifier</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;legalPersonName</td>
        <td>CBA Trading</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;legalPersonNameIdentifierType</td>
        <td>TRAD</td>
    </tr>
    <tr>
        <td>accountNumber</td>
        <td>00010190CBATRAD</td>
    </tr>
    <tr>
        <td><h6>PayloadMetadata</h6></td>
    </tr>
    <tr>
        <td>transliterationMethod</td>
        <td>hani</td>
    </tr>
    <tr>
        <td><h6>TransferPath</h6></td>
    </tr>
    <tr>
        <td>transferPath</td>
        <td></td>
    </tr>
    <tr>
        <td>intermediaryVASP</td>
        <td></td>
    </tr>
    <tr>
        <td>legalPerson</td>
        <td></td>
    </tr>
    <tr>
        <td>name*</td>
        <td></td>
    </tr>
    <tr>
        <td>nameIdentifier*</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;legalPersonName</td>
        <td>VASP E</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;legalPersonNameIdentifierType</td>
        <td>LEGL</td>
    </tr>
    <tr>
        <td>sequence</td>
        <td>0</td>
    </tr>
</table>
<br><br><br>

---
<br>

<sup>1</sup> The FATF Recommendations, Updated June 2019, pg. 71. <a id="ref-1"></a>

<sup>2</sup> The model is used for illustrative purposes and is not intended to define the underlying structure or technologies used for any such platform. <a id="ref-2"></a>

<sup>3</sup> Adapted from International Civil Aviation Organization (ICAO) Doc 9303, Machine Readable Travel Documents, Seventh Edition, 2015 Part 3: Specifications Common to all MRTDs, Section 3.4 Convention for Writing the Name of the Holder. <a id="ref-3"></a>

<sup>4</sup> Adapted from International Civil Aviation Organization (ICAO) Doc 9303, Machine Readable Travel Documents, Seventh Edition, 2015 Part 3: Specifications Common to all MRTDs, Section 3.4 Convention for Writing the Name of the Holder. <a id="ref-4"></a>

<sup>5</sup> Adapted from ISO20022 registered message component PostalAddress6. <a id="ref-5"></a>

<sup>6</sup>  Adapted from International Civil Aviation Organization (ICAO) Doc 9303, Machine Readable Travel Documents, Seventh Edition, 2015 Part 3: Specifications Common to all MRTDs, Section 3.7 Representation of Place of Birth. <a id="ref-6"></a>

<sup>7</sup>  Additional ‘Address Type’ value for the purposes of IVMS101; covers any physical / geographic address type held by the VASP on their client. <a id="ref-7"></a>

<sup>8</sup>  World International Property Organisation Standard ST.3 Recommended standard on two-letter codes for the representation of states, other entities and intergovernmental organizations, Paragraph 6. <a id="ref-8"></a>

<sup>9</sup>  Registration Authority Code, as published by the Global Legal Entity Identifier Foundation (GLEIF), Registration Authorities List. <a id="ref-9"></a>

<sup>10</sup>  NationalIdentifierTypeCode applies a restriction over the codes present in ISO20022 datatype ‘TypeOfIdentification4Code’. <a id="ref-10"></a>

<sup>11</sup>  The LEI is a 20-character, alpha-numeric code that enables clear and unique identification of legal entities participating in financial transactions. <a id="ref-11"></a>

<sup>12</sup>  Datatype list ‘TransliterationMethodCode’ uses the four-letter script code as presented in ISO 15924, Codes for the representation of names of scripts. <a id="ref-12"></a>

<sup>13</sup>  Adapted from International Civil Aviation Organization (ICAO) Doc 9303, Machine Readable Travel Documents, Seventh Edition, 2015 Part 3: Specifications Common to all MRTDs, Section 3.1 Languages and Characters  <a id="ref-13"></a>

<sup>14</sup> National Institute of Korean Language, Romanization of Korean
<a href='https://www.korean.go.kr/front_eng/roman/roman_01.do' target='_blank'>https://www.korean.go.kr/front_eng/roman/roman_01.do</a> <a id="ref-14"></a>

<sup>15</sup>  In Example 1, the method selected by the VASP to identify the originator is the Geographic Address.  <a id="ref-15"></a>

<sup>16</sup>  6 In Example 2, the method selected by the VASP to identify the originator is the National Identifier.  <a id="ref-16"></a>
